from flask import Flask, jsonify, request, Response
import backend.operations as ops
import backend.models as models
import configparser
import base64

config = configparser.ConfigParser()
config.read('../.env')

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = config.get('DATABASE', 'DB_DEV_URI')
app.config['BCRYPT_HANDLE_LONG_PASSWORDS'] = True

models.db.init_app(app)
ops.bcrypt.init_app(app)


@app.route('/asare/api/v1/users/login', methods=["POST"])
def handle_user():
    body = request.json

    with app.app_context():
        response = ops.get_user(body['email'], body['password'])

    if not response:
        return Response(response='Invalid User Password', status=401)

    if response is None:
        return Response(response='Internal Server Error', status=500)

    return jsonify(response)


@app.route('/asare/api/v1/users/signup', methods=["POST"])
def post_user():
    body = request.json

    with app.app_context():
        byte_str = body['password'].encode("ascii")

        result_ok = ops.post_user(models.db, body['username'], body['email'],
                                  base64.b64decode(byte_str).decode('ascii'), '{ "some":"test"')

    if not result_ok:
        return Response(response='Internal Server Error', status=500)

    return Response(response='User created successfully', status=200)


@app.route('/asare/api/v1/users/approximations/<string:user_uuid>', methods=['GET'])
def get_user_approximations(user_uuid: str):
    with app.app_context():
        user_approximations = ops.get_user_approximations(user_uuid)

    if user_approximations is None:
        return Response(response='Internal Server Error', status=500)

    if len(user_approximations) == 0:
        return Response(response='Approximations not found', status=404)

    return jsonify(user_approximations)


@app.route('/asare/api/v1/users/approximations', methods=["POST"])
def post_user_approximation():
    body = request.json

    records = body.pop('records', None)

    approximation = models.Approximations(**body)

    with app.app_context():
        result_ok = ops.post_user_approximation(models.db, approximation, records)

    if not result_ok:
        return Response(response='Internal Server Error', status=500)

    return Response(response="Approximations created successfully", status=200)


@app.route('/asare/api/v1/users/records/<string:user_uuid>', methods=['GET'])
def get_user_records(user_uiid: str):
    with app.app_context():
        user_records = ops.get_user_records(user_uiid)

    if user_records is None:
        return Response(status=500)

    if len(user_records) == 0:
        return Response(response='Records not found', status=404)

    return jsonify(user_records)


@app.route('/asare/api/v1/users/subusers/<string:user_uuid>', methods=["GET"])
def get_user_subusers(user_uuid: str):
    with app.app_context():
        user_subusers = ops.get_user_subusers(user_uuid)

    if user_subusers is None:
        return Response(status=500)

    if len(user_subusers) == 0:
        return Response(response='Subusers not found', status=404)

    return jsonify(user_subusers)


@app.route('/asare/api/v1/users/subusers', methods=["POST"])
def post_subuser():
    body = request.json

    with app.app_context():
        new_subuser = models.SubUsers(**body)
        result_ok = ops.post_subuser(models.db, new_subuser)

    if not result_ok:
        return Response(status=500)

    return Response(response='Subuser created successfully', status=200)


if __name__ == "__main__":
    app.run(debug=True)
