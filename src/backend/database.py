"""
Script for creating the model tables in the database
"""
from models import db 
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
import os 
import configparser


config = configparser.ConfigParser()
config.read('../../.env')

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = config.get('DATABASE', 'DB_DEV_URI')

db.init_app(app)

if __name__ == "__main__":
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])

    if not database_exists(engine.url):
        create_database(engine.url)

    with app.app_context():
        db.create_all()

