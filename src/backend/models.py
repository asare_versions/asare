from sqlalchemy.dialects.postgresql import UUID
from flask_sqlalchemy import SQLAlchemy
import enum

db = SQLAlchemy()


class Users(db.Model):
    idUser = db.Column(UUID(as_uuid=True), primary_key=True)
    username = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    privileges = db.Column(db.JSON, nullable=False)

    # Relationships
    subUsers = db.relationship('SubUsers', backref='users', lazy=True)
    approximations = db.relationship('Approximations', backref='users', lazy=True)

    @property
    def serialize(self):
        return {
            'idUser': self.idUser, 'username': self.username, 'email': self.email,
            'privileges': self.privileges
        }


class SubUsers(db.Model):
    idSubUser = db.Column(db.BigInteger, primary_key=True)
    idUser = db.Column(UUID(as_uuid=True), db.ForeignKey('users.idUser'), nullable=False)
    username = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    privileges = db.Column(db.JSON, nullable=False)

    @property
    def serialize(self):
        return {
            'idSubUser': self.idSubUser, 'idUser': self.idUser, 'username': self.username,
            'privileges': self.privileges
        }


class Approximations(db.Model):
    idAprox = db.Column(db.BigInteger, primary_key=True)
    idUser = db.Column(UUID(as_uuid=True), db.ForeignKey('users.idUser'), nullable=False)
    approximationName = db.Column(db.String(255), nullable=False)
    totalCost = db.Column(db.Numeric(18, 2), nullable=False)
    numberMaterials = db.Column(db.Integer, nullable=False)
    numberServices = db.Column(db.Integer, nullable=False)
    description = db.Column(db.Text, nullable=False)
    date = db.Column(db.Date, nullable=False)

    # Relationships
    records = db.relationship('Records', backref='approximations', lazy=True)

    @property
    def serialized(self):
        return {
            'idAprox': self.idAprox, 'idUser': self.idUser, 'approximationName': self.approximationName,
            'totalCost': self.totalCost, 'numberMaterials': self.numberMaterials, 'numberServices': self.numberServices,
            'description': self.description, 'date': str(self.date)
        }


# Enum for representing the record type of Record model
class RecordType(enum.Enum):
    MATERIAL = 0
    SERVICE = 1


class Records(db.Model):
    idRecord = db.Column(db.BigInteger, primary_key=True)
    idAprox = db.Column(db.BigInteger, db.ForeignKey('approximations.idAprox'), nullable=False)
    recordname = db.Column(db.String(255), nullable=False)
    unitCost = db.Column(db.Numeric(18, 2), nullable=False)
    description = db.Column(db.Text, nullable=False)
    amount = db.Column(db.Integer, nullable=False)
    recordType = db.Column(db.Enum(RecordType), nullable=False)

    @property
    def serialize(self):
        return {
            'idRecord': self.idRecord, 'idAprox': self.idAprox, 'recordname': self.recordname,
            'unitCost': self.unitCost,
            'description': self.description, 'amount': self.amount,
            'recordType': 'MATERIAL' if self.recordType == RecordType.MATERIAL else 'SERVICE'
        }
