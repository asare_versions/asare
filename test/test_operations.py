import configparser
import unittest
from backend.models import Users, db, Approximations, Records, RecordType 
from backend.operations import bcrypt, get_user, post_user, get_user_subusers, post_subuser, post_user_approximation, get_user_approximations, get_user_records
from flask import Flask

config = configparser.ConfigParser()
config.read('.env')

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = config.get('DATABASE', 'DB_TEST_URI')
app.config['BCRYPT_HANDLE_LONG_PASSWORDS'] = True

db.init_app(app)
bcrypt.init_app(app)

class TestOperations(unittest.TestCase):

    def test_post_user(self):
        test_username = "Camille"
        test_email = "camille@mail.com"
        test_password = "123"
        test_privileges = '{ "some":"privileges"}' 
    
        with app.app_context():
            self.assertTrue(post_user(db, test_username, test_email, test_password, test_privileges))


    def test_get_user(self):
        test_email = "camille@mail.com"

        with app.app_context():
            self.assertEqual(test_email, get_user(test_email, "123").email)
    

    def test_get_user_subusers(self):
        test_username = "User"
        test_uuid = "15e3316c-0a78-43c3-bbce-02e7f4b2defe"
        id = 1
        result = {} 

        with app.app_context():
            result = get_user_subusers(test_uuid)
         

        self.assertEqual(test_username, result[id].username)

    def test_post_subuser(self):

        id = "15e3316c-0a78-43c3-bbce-02e7f4b2defe" 
        test_username = "User"
        test_password = "321"
        test_privileges = '{"some":"privileges"}'
        
        with app.app_context():
            self.assertTrue(post_subuser(db, id, test_username, test_password, test_privileges))


    def test_get_user_approximations(self):
        test_uuid = '15e3316c-0a78-43c3-bbce-02e7f4b2defe'
        test_name = 'Test'
        
        result = {}

        with app.app_context():
            result = get_user_approximations(test_uuid)
        
        self.assertEqual(test_name, result[1][0].approximationName)
        self.assertEqual(2, len(result[1][1]))


    def test_post_user_approximation(self):
        id = "15e3316c-0a78-43c3-bbce-02e7f4b2defe"
        approx = Approximations(idUser=13, approximationName="Test", totalCost=13.24, numberMaterials=1, numberServices=2, description='Some', date='07/03/23')
        records = [Records(recordname="Test1", unitCost=12.23, description='SOme', amount=2, recordType=RecordType.MATERIAL), Records(recordname="Test2", unitCost=12.25, description='SOme', amount=1, recordType=RecordType.SERVICE)]

        with app.app_context():
            self.assertTrue(post_user_approximation(db, id, approx, records))          
    

    def test_get_user_records(self):
        test_uuid = '15e3316c-0a78-43c3-bbce-02e7f4b2defe'     
        test_name = "Test1"

        result = {}
        
        with app.app_context():
            result = get_user_records(test_uuid)


        self.assertEqual(test_name, result[0].recordname)
        self.assertEqual(2, len(result))
        


if __name__ == "__main__":
    unittest.main()

